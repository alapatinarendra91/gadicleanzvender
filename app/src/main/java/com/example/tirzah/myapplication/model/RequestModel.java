package com.example.tirzah.myapplication.model;

/**
 * Created by i5 on 02-10-2018.
 */

public class RequestModel {
    private String mobileoremail;
    private String password;

    public String getMobileoremail() {
        return mobileoremail;
    }

    public void setMobileoremail(String mobileoremail) {
        this.mobileoremail = mobileoremail;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
