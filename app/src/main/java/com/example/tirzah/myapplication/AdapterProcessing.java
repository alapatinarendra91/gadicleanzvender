package com.example.tirzah.myapplication;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.example.tirzah.myapplication.utils.CommonUtils;

import java.util.List;

public class AdapterProcessing extends RecyclerView.Adapter<AdapterProcessing.InformationViewHolder>  {
    private Context context;
    private List<Information> information;

    public AdapterProcessing(Context context, List<Information> information) {
        this.context = context;
        this.information = information;
    }

    @NonNull
    @Override
    public InformationViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        LayoutInflater inflater=LayoutInflater.from(context);
        View view=inflater.inflate(R.layout.processinglist,null);
        InformationViewHolder holder=new InformationViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull InformationViewHolder informationViewHolder, final int i) {
        informationViewHolder.TextName.setText(CommonUtils.fromHtml("Name: ","Murali Krishna"));
        informationViewHolder.TextService.setText(CommonUtils.fromHtml("Service: ","Full Car Wash"));
        informationViewHolder.Textdate.setText(CommonUtils.fromHtml("Date: ","01/01/2018"));
        informationViewHolder.TextStatus.setText(CommonUtils.fromHtml("Status: ","Awaiting Driver Confirmation"));
        informationViewHolder.recyclerViewprocessing.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(context,"Position" + i,Toast.LENGTH_LONG).show();
            }
        });
    }

    @Override
    public int getItemCount() {
        return 10;
    }

    class InformationViewHolder extends RecyclerView.ViewHolder{
        TextView TextName,TextService,TextStatus,Textdate;
        RecyclerView recyclerViewprocessing;

        public InformationViewHolder(@NonNull View itemView) {
            super(itemView);
            TextName=itemView.findViewById(R.id.nameprocessing);
            TextService=itemView.findViewById(R.id.serviceprocessing);
            TextStatus=itemView.findViewById(R.id.statusprocessing);
            Textdate=itemView.findViewById(R.id.dateprocessing);
            recyclerViewprocessing=itemView.findViewById(R.id.recyclerviewprocessing);

        }
    }
}
