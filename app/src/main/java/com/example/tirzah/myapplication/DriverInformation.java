package com.example.tirzah.myapplication;

public class DriverInformation {
    private int id;
   

    private String driverName,driverLocation;
    private String Assignbtn,Rejectbtn;

    public int getId() {
        return id;
    }

    public String getDriverName() {
        return driverName;
    }

    public String getDriverLocation() {
        return driverLocation;
    }

    public String getAssignbtn() {
        return Assignbtn;
    }

    public String getRejectbtn() {
        return Rejectbtn;
    }

    public DriverInformation(int id, String driverName, String driverLocation, String assignbtn, String rejectbtn) {
        this.id = id;
        this.driverName = driverName;
        this.driverLocation = driverLocation;
        Assignbtn = assignbtn;
        Rejectbtn = rejectbtn;
    }
}
