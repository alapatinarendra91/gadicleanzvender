package com.example.tirzah.myapplication;


import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.List;


/**
 * A simple {@link Fragment} subclass.
 */
public class Processing extends Fragment {
    List<Information> informationList;
    AdapterProcessing adapterProcessing;
    RecyclerView recyclerView;



    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view=inflater.inflate(R.layout.fragment_processing, container, false);
        informationList=new ArrayList<Information>();
        recyclerView=(RecyclerView)view.findViewById(R.id.recyclerviewprocessing);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));

        informationList.add(new Information(1,
                "Murali Krishna",
                "Full car service",
                "01/01/2018",
                "Awaiting Driver Conformation"));
        adapterProcessing= new AdapterProcessing(getActivity(),informationList);
        recyclerView.setAdapter(adapterProcessing);


        return view;
    }

    public interface OnFragmentInteractionListener {
    }
}
