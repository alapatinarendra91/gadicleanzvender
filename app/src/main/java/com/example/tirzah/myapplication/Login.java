package com.example.tirzah.myapplication;

import android.content.Context;
import android.content.Intent;
import android.support.design.widget.TextInputEditText;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.textclassifier.TextLinks;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.example.tirzah.myapplication.constants.AppConstants;
import com.example.tirzah.myapplication.constants.ErrorModel;
import com.example.tirzah.myapplication.constants.Event;
import com.example.tirzah.myapplication.constants.VolleyErrorListener;
import com.example.tirzah.myapplication.constants.WebServices;
import com.example.tirzah.myapplication.model.CommonRootModel;
import com.example.tirzah.myapplication.model.RequestModel;
import com.example.tirzah.myapplication.ui.activity.CarWashBaseActivity;
import com.example.tirzah.myapplication.utils.AlertDialogHelper;
import com.example.tirzah.myapplication.utils.CommonUtils;
import com.example.tirzah.myapplication.utils.PrefUtil;
import com.google.gson.Gson;

import java.util.HashMap;
import java.util.Map;

import carwash.ext.GsonObjectRequest;
import carwash.ext.RequestManager;
import carwash.utils.ToastUtils;

public class Login extends CarWashBaseActivity implements AlertDialogHelper.AlertDialogListener {
    private Context mContext;
    private long backPressedTime;
    private EditText email,password;
    private AlertDialogHelper alertDialog;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        mContext = this;
        alertDialog = new AlertDialogHelper(mContext);
        alertDialog.setOnAlertListener(this);
        email = findViewById(R.id.text_email);
        password = findViewById(R.id.text_Password);

    }


    public boolean validateEmail(){
        String Email = email.getText().toString().trim();
        if(Email.isEmpty()){
            email.setError("Feild Cant be empty");
            return false;
        }else {
            email.setError(null);
            return true;
        }
    }

    public boolean validatepassword(){
        String Password = password.getText().toString().trim();
        if(Password.isEmpty()){
            password.setError("Feild Cant be empty");
            return false;
        }else {
            password.setError(null);
            return true;
        }
    }

    public void Login(View view){
        if (validateEmail() && validatepassword()){
            getData(Event.vendorsignin);
            return;
        }


    }
    public void forgot(View view){
        if (validateEmail()) {
            alertDialog.showAlertDialog("Forgot Password", "Are you sure you want to get your password to "+ email.getText().toString() +"  email?", "Yes", "No", Event.vendorforgotpassword, true);

        }

//        Intent intent=new Intent(this,ForgotPassward.class);
//        startActivity(intent);
    }

    @Override
    public void getData(int actionID) {
        showProgressDialog(getString(R.string.please_wait), false);
        HashMap<String, String> reqHeader = new HashMap<>();
        reqHeader.put("Content-Type", "application/json; charset=utf-8");
        String request = "";
        Map<String,String> params = new HashMap<String, String>();
        switch (actionID) {
            case Event.vendorsignin:
                request = sendLoginRequest();
                CommonUtils.logPrint("TAG", "request : " + request);
                RequestManager.addRequest(new GsonObjectRequest<CommonRootModel>
                        (WebServices.vendorsignin, params, request, CommonRootModel.class,
                                new VolleyErrorListener(this, Event.vendorsignin)) {
                    @Override
                    protected void deliverResponse(CommonRootModel response) {
                        updateUi(true, Event.vendorsignin, response);
                    }
                });
                break;

            case Event.vendorforgotpassword:
                request = sendForgotPasswordRequest();
                CommonUtils.logPrint("TAG", "request : " + request);
                RequestManager.addRequest(new GsonObjectRequest<CommonRootModel>
                        (WebServices.vendorforgotpassword, params, request, CommonRootModel.class,
                                new VolleyErrorListener(this, Event.vendorforgotpassword)) {
                    @Override
                    protected void deliverResponse(CommonRootModel response) {
                        updateUi(true, Event.vendorforgotpassword, response);
                    }
                });
                break;

        }
    }

    @Override
    public void updateUi(boolean status, int action, Object serviceResponse) {
        removeProgressDialog();
        if (status) {
            switch (action) {
                case Event.vendorsignin:
                    CommonRootModel mLoginModel = (CommonRootModel) serviceResponse;
                    if (mLoginModel.getStatusCode() == 200){
                        PrefUtil.putString(mContext, AppConstants.PREF_customer_id, mLoginModel.getData().getUser_id(), AppConstants.PREF_NAME);
                        PrefUtil.putString(mContext, AppConstants.PREF_user_email, mLoginModel.getData().getUser_email(), AppConstants.PREF_NAME);
                        PrefUtil.putBool(mContext, AppConstants.PREF_firstTimeLogin, true, AppConstants.PREF_NAME);
                        startActivity(new Intent(mContext,MainActivity.class));
                        finish();
                    }
                    break;

                case Event.vendorforgotpassword:
                    CommonRootModel mForGotModel = (CommonRootModel) serviceResponse;
                    ToastUtils.showToast(mContext, "" + mForGotModel.getStatusText());
                    if (mForGotModel.getStatusCode() == 200){
                        PrefUtil.putString(mContext, AppConstants.PREF_customer_id, mForGotModel.getCustomerId(), AppConstants.PREF_NAME);
//                        startActivity(new Intent(mContext,ChangePasswordActivity.class).putExtra(AppConstants.intent_ComingFrom,AppConstants.intent_loginScreen));
                    }
                    break;

            }
        } else {
            ErrorModel response = (ErrorModel) serviceResponse;
            ToastUtils.showToast(mContext, "" + response.getStatusText());

        }
    }



    private String sendLoginRequest() {
        RequestModel mModel = new RequestModel();
        mModel.setMobileoremail(email.getText().toString());
        mModel.setPassword(password.getText().toString());
        return new Gson().toJson(mModel);
    }

    private String sendForgotPasswordRequest() {
        RequestModel mModel = new RequestModel();
        mModel.setMobileoremail(email.getText().toString());
        return new Gson().toJson(mModel);
    }

    @Override
    public void onPositiveClick(int from)
    {
        getData(Event.vendorforgotpassword);
    }

    @Override
    public void onNegativeClick(int from) {

    }

    @Override
    public void onNeutralClick(int from) {

    }
    @Override
    public void onBackPressed() {

        if (backPressedTime + 2000 > System.currentTimeMillis()){
            super.onBackPressed();
            return;
        }else {
            Toast.makeText(getBaseContext(),"Press back again to exit",Toast.LENGTH_LONG).show();
        }
        backPressedTime = System.currentTimeMillis();
    }
}
