package com.example.tirzah.myapplication;


import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.ContextMenu;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;

import java.util.ArrayList;
import java.util.List;

import static com.example.tirzah.myapplication.R.id.Driveradd;


/**
 * A simple {@link Fragment} subclass.
 */
public class Drivers extends Fragment{
    List<DriverInformation> driverInformations;
    RecyclerView recyclerView;
    DriverAdaptor adaptor;



    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_drivers, container, false);
        FloatingActionButton actionButton=(FloatingActionButton)view.findViewById(R.id.floting);
        ImageButton notification=(ImageButton)view.findViewById(R.id.imagebutton_drivers_notification);
        notification.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent=new Intent(getActivity(),Notifications.class);
                startActivity(intent);

            }
        });
        actionButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(getActivity(),DriverRegistration.class);
                startActivity(intent);
            }
        });
        recyclerView = (RecyclerView) view.findViewById(R.id.driverRecyclerview);
        driverInformations = new ArrayList<DriverInformation>();
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        driverInformations.add(new DriverInformation(1,
                "Manoj Kumar",
                "Dilshuknagar",
                "Assign",
                "Reject"));
        adaptor = new DriverAdaptor(getActivity(), driverInformations);
        recyclerView.setAdapter(adaptor);


        return view;
    }



}
