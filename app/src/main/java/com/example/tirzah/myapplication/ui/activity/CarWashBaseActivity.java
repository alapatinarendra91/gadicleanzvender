package com.example.tirzah.myapplication.ui.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;

import com.example.tirzah.myapplication.R;
import com.example.tirzah.myapplication.ui.fragment.CarWashBaseFragment;

import carwash.ui.activity.BaseActivity;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class CarWashBaseActivity extends BaseActivity {
    private CarWashBaseFragment mGetAtBaseFragment;
    String TAG = "GetAtBaseActivity";

    public void setGetAtBaseFragment(Fragment mGetAtBaseFragment) {
        if (mGetAtBaseFragment instanceof CarWashBaseFragment) {
            this.mGetAtBaseFragment = (CarWashBaseFragment) mGetAtBaseFragment;
            setOnBackHandle((OnBackHandle) mGetAtBaseFragment);
        }
    }

    @Override
    public void updateUi(boolean status, int action, Object serviceResponse) {

    }

    @Override
    public void onEvent(int eventId, Object eventData) {

    }

    @Override
    public void getData(int actionID) {

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    protected int getContentViewId() {
        return 0;
    }


//    @Override
//    protected int getContentViewId() {
//        return R.layout.activity_main;
//    }

    /***
     * Device Back Button handler
     */

    /*@Override
    public void onBackPressed() {
        FragmentManager manager = getSupportFragmentManager();

        CommonUtils.showToast(getApplicationContext(),"BaCkPressed");

        FragmentManager fm = getSupportFragmentManager();
        if (fm.getBackStackEntryCount() >= 1) {
            fm.popBackStack(2, FragmentManager.POP_BACK_STACK_INCLUSIVE);
        }

        *//**Check for click on any fragment,if fragment onBackPressed return_icon true than, we take our custom action on device back button.*//*
//        if (mGetAtBaseFragment != null && mGetAtBaseFragment.onBackPressed()) {
//            return_icon;
//            *//** Check if current page is Home page than tap on back button application is shut down.*//*
//        } else if (mGetAtBaseFragment instanceof ExploreFragment) {
//            Intent intent = new Intent(ACTION_DEATH_REGISTER_COMPLETE);
//            LocalBroadcastManager.getInstance(this).sendBroadcast(intent);
//            *//**Check if only one fragment in stack and its not a HomePage fragment than on Back Button tap add the home page.*//*
//        } else if (manager.getBackStackEntryCount() == 1 && (mGetAtBaseFragment instanceof WebViewHintSix)) {
//            finish();
//
//        }  else if (manager.getBackStackEntryCount() == 1 && !(mGetAtBaseFragment instanceof ExploreFragment)) {
//            Fragment fragment = new ExploreFragment();
//            addFragment(fragment, null, true, false, false, null, R.id.containerView);
//            return_icon;
//            *//**If there is many fragment in stack than remove the top most from stack.*//*
//        } else if (manager.getBackStackEntryCount() > 1 && !(mGetAtBaseFragment instanceof ExploreFragment)) {
//            manager.popBackStack();
//        } else {
//            *//**Other wise call the parent activity back press method*//*
//            super.onBackPressed();
//        }
    }*/


    /**
     * Replace a fragment in containerId
     *
     * @param fragment       Fragment
     * @param bundle         Bundle if you don't pass bundle to next fragment then pass null
     * @param addToBackStack boolean if you want to maintain back then pass true
     * @param anim           boolean if you want to animate fragment from bottom then pass true
     * @param retainInstance false;
     * @param containerId    R.id.container
     */
    public void replaceFragment(Fragment fragment, Bundle bundle, boolean addToBackStack, boolean anim, boolean retainInstance, Fragment sourceFragment, int containerId) {
        if (bundle != null) fragment.setArguments(bundle);
        FragmentManager fragmentManager = getSupportFragmentManager();
        String tag = fragment.getClass().getSimpleName();
        Fragment already = fragmentManager.findFragmentByTag(tag);
        boolean fragmentPopped = fragmentManager.popBackStackImmediate(tag, 0);
        FragmentTransaction ft = fragmentManager.beginTransaction();

        if (anim) {
            ft.setCustomAnimations(R.anim.slide_from_right, 0, 0, R.anim.slide_to_right);
        }
        ft.replace(containerId, fragment, tag);
        ft.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
        fragment.setRetainInstance(retainInstance);
        if (addToBackStack && !fragmentPopped && already == null)
            ft.addToBackStack(tag);
        if (sourceFragment != null) ft.hide(sourceFragment);
        try {
            ft.commit();
        } catch (Exception ex) {
            ex.printStackTrace();
        }

        setGetAtBaseFragment(fragment);
    }

    /**
     * Add a fragment in R.id.container
     *
     * @param fragment        Fragment
     * @param bundle          Bundle if you don't pass bundle to next fragment then pass null
     * @param addToBackStack  boolean if you want to maintain back then pass true
     * @param currentFragment Fragment
     */

    public void addFragment(Fragment fragment, Bundle bundle, boolean addToBackStack, boolean anim, boolean retainInstance, Fragment currentFragment, int containerId) {
        if (bundle != null) fragment.setArguments(bundle);
        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction ft = fragmentManager.beginTransaction();
        String tag = fragment.getClass().getSimpleName();
        Fragment already = fragmentManager.findFragmentByTag(tag);
        fragmentManager.popBackStackImmediate(tag, 0);
        if (already == null) {
            if (anim) {
                ft.setCustomAnimations(R.anim.slide_from_right, 0, 0, R.anim.slide_to_right);
            }
            ft.add(containerId, fragment, tag);
            fragment.setRetainInstance(retainInstance);
            if (addToBackStack)
                ft.addToBackStack(tag);
            if (currentFragment != null) ft.hide(currentFragment);
            try {
                ft.commit();
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        } else {
            fragment = already;
            CarWashBaseFragment baseFragment = (CarWashBaseFragment) fragment;
        }
        setGetAtBaseFragment(fragment);
    }

    /***
     * Add fragment with custom tag
     *
     * @param fragment
     * @param bundle
     * @param addToBackStack
     * @param anim
     * @param retainInstance
     * @param currentFragment
     * @param containerId
     * @param tag
     */
    public void addRecentFragment(Fragment fragment, Bundle bundle, boolean addToBackStack, boolean anim, boolean retainInstance, Fragment currentFragment, int containerId, String tag) {
        if (bundle != null) fragment.setArguments(bundle);
        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction ft = fragmentManager.beginTransaction();
        Fragment already = fragmentManager.findFragmentByTag(tag);
        fragmentManager.popBackStackImmediate(tag, 0);
        if (already == null) {
            if (anim) {
                ft.setCustomAnimations(R.anim.slide_from_right, 0, 0, R.anim.slide_to_right);
            }
            ft.add(containerId, fragment, tag);
            fragment.setRetainInstance(retainInstance);
            if (addToBackStack)
                ft.addToBackStack(tag);
            if (currentFragment != null) ft.hide(currentFragment);
            try {
                ft.commit();
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        } else {
            fragment = already;
            CarWashBaseFragment baseFragment = (CarWashBaseFragment) fragment;
        }
        setGetAtBaseFragment(fragment);
    }

    /**
     * Pop back all frgaments from stack
     */
    public void cleanFrgamentBackStack() {
        FragmentManager manager = getSupportFragmentManager();
        if (manager.getBackStackEntryCount() > 0) {
            FragmentManager.BackStackEntry first = manager.getBackStackEntryAt(0);
            manager.popBackStack(first.getId(), FragmentManager.POP_BACK_STACK_INCLUSIVE);
        }
    }

    /**
     * clear fragment stack
     */
    public void clearAllStack() {
        final FragmentManager fragmentManager = getSupportFragmentManager();
        while (fragmentManager.getBackStackEntryCount() != 0) {
            fragmentManager.popBackStackImmediate();
        }
    }

    /**
     * clear fragment stack
     */
    public void clearStack() {
        final FragmentManager fragmentManager = getSupportFragmentManager();

        while (fragmentManager.getBackStackEntryCount() != 0) {
            if (fragmentManager.getBackStackEntryCount() > 1) {
                fragmentManager.popBackStackImmediate();
            } else {
                break;
            }

        }

    }

    /**
     * Get current fragment in containerId
     *
     * @param containerId = R.id.container
     */
    public Fragment getFragment(int containerId) {
        return getSupportFragmentManager().findFragmentById(containerId);
    }

    /***
     * Fragment back handling listener
     */
    public interface OnBackHandle {
        /***
         * Device Back button control on fragment
         *
         * @return
         */
        boolean onBackPressed();

        /**
         * Update title of fragment
         */
//        void updateTitle();

        /***
         * take activity result on fragment
         *
         * @param requestCode
         * @param resultCode
         * @param data
         */
        void onMyActivityResult(int requestCode, int resultCode, Intent data);
    }

    public OnBackHandle onBackHandle;

    public void setOnBackHandle(OnBackHandle onBackHandle) {
        this.onBackHandle = onBackHandle;
    }

    /***
     * Get Application class instance
     *
     * @return
     */
    public CarWashBaseApplication getAtApplication() {
        return (CarWashBaseApplication) getApplication();
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }
}

