package com.example.tirzah.myapplication;

public class InformationConfirmed {
    private String id;
    private String Name;
    private String Service;
    private String Price;
    private String date;
    private String Status;

    public String getId() {
        return id;
    }

    public String getName() {
        return Name;
    }

    public String getService() {
        return Service;
    }

    public String getPrice() {
        return Price;
    }

    public String getDate() {
        return date;
    }

    public String getStatus() {
        return Status;
    }

    public InformationConfirmed(String id, String name, String service, String price, String date, String status) {
        this.id = id;
        Name = name;
        Service = service;
        Price = price;
        this.date = date;
        Status = status;
    }
}
