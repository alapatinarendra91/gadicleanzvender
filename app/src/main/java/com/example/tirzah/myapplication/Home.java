package com.example.tirzah.myapplication;


import android.content.Intent;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.RequiresApi;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;

import com.baoyachi.stepview.VerticalStepView;

import java.util.ArrayList;
import java.util.List;


/**
 * A simple {@link Fragment} subclass.
 */
public class Home extends Fragment {
    VerticalStepView verticalStepView;

    public Home() {
        // Required empty public constructor
    }


    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        // Inflate the layout for this fragment
        View view=inflater.inflate(R.layout.fragment_home, container, false);
        verticalStepView = view.findViewById(R.id.verticalstepView);
        List<String> Sources = new ArrayList<>();
        Sources.add("Confirming Order");
        Sources.add("Driver is on the way");
        Sources.add("Driver has pickeup your car");
        Sources.add("Car has been recived to service");
        Sources.add("Car has been delivered");

        verticalStepView.setStepsViewIndicatorComplectingPosition(Sources.size()-5)
                .reverseDraw(false).setStepViewTexts(Sources)
                .setLinePaddingProportion(1f)
                .setStepsViewIndicatorCompletedLineColor(Color.parseColor("#f17326"))
                .setStepsViewIndicatorUnCompletedLineColor(Color.parseColor("#FF000000"))
                .setStepViewComplectedTextColor(Color.parseColor("#f17326"))
                .setStepViewUnComplectedTextColor(ContextCompat.getColor(getActivity(),R.color.black))
                .setStepsViewIndicatorUnCompletedLineColor(Color.parseColor("#f17326"))
                .setStepsViewIndicatorCompleteIcon(ContextCompat.getDrawable(getActivity(),R.drawable.ic_local_car_wash_black_24dp))
                .setStepsViewIndicatorAttentionIcon(ContextCompat.getDrawable(getActivity(),R.drawable.ic_local_car_wash_black_24dp))
                .setStepsViewIndicatorDefaultIcon(ContextCompat.getDrawable(getActivity(),R.drawable.default_icon));
        ImageButton notification=(ImageButton)view.findViewById(R.id.imageButton_home_notification);
        notification.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent=new Intent(getActivity(),Notifications.class);
                startActivity(intent);

            }
        });

        return view;
    }

}
