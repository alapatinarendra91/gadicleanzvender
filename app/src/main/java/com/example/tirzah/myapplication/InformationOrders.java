package com.example.tirzah.myapplication;

public class InformationOrders {
    private int id;
    private String textOrderName, textOrderService, textOrderLocation;
    private double textOrderprice;
    private String Acceptbtn, RejectBtn;

    public int getId() {
        return id;
    }

    public String getTextOrderName() {
        return textOrderName;
    }

    public String getTextOrderService() {
        return textOrderService;
    }

    public String getTextOrderLocation() {
        return textOrderLocation;
    }

    public double getTextOrderprice() {
        return textOrderprice;
    }

    public String getAcceptbtn() {
        return Acceptbtn;
    }

    public String getRejectBtn() {
        return RejectBtn;
    }

    public InformationOrders(int id, String textOrderName, String textOrderService, String textOrderLocation, double textOrderprice, String acceptbtn, String rejectBtn) {
        this.id = id;
        this.textOrderName = textOrderName;
        this.textOrderService = textOrderService;
        this.textOrderLocation = textOrderLocation;
        this.textOrderprice = textOrderprice;
        Acceptbtn = acceptbtn;
        RejectBtn = rejectBtn;
    }
}