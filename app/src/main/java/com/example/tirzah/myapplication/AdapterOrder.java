package com.example.tirzah.myapplication;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.example.tirzah.myapplication.utils.CommonUtils;

import java.util.List;

public class AdapterOrder extends RecyclerView.Adapter<AdapterOrder.OrderViewhlder> {
    private Context context;
    private List<InformationOrders> informationOrders;

    public AdapterOrder(Context context, List<InformationOrders> informationOrders) {
        this.context = context;
        this.informationOrders = informationOrders;
    }

    @NonNull
    @Override
    public OrderViewhlder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        LayoutInflater inflater=LayoutInflater.from(context);
        View view=inflater.inflate(R.layout.my_order_list_layout,null);
        OrderViewhlder viewhlder=new OrderViewhlder(view);
        return viewhlder;
    }

    @Override
    public void onBindViewHolder(@NonNull OrderViewhlder orderViewhlder, final int i) {
        orderViewhlder.textordername.setText(CommonUtils.fromHtml("Name: ","Ashok Kumar" ));
        orderViewhlder.textorderservice.setText(CommonUtils.fromHtml("Service: ","Full Car Service" ));
        orderViewhlder.textorderprice.setText(CommonUtils.fromHtml("Price:","2000" ));
        orderViewhlder.textorderlocation.setText(CommonUtils.fromHtml("Location: ","Kukatpally" ));
//        orderViewhlder.recyclerOverrideVieworder.setOnClickListener(new View.OnClickListener() {
//            @
//            public void onClick(View v) {
//                Toast.makeText(context,"Position" + i,Toast.LENGTH_LONG).show();
//            }
//        });
    }

    @Override
    public int getItemCount() {
        return 10;
    }

    class OrderViewhlder extends RecyclerView.ViewHolder{
        TextView textordername,textorderservice,textorderprice,textorderlocation;
        Button acceptbtn,rejectedbtn;
//        RecyclerView recyclerVieworder;


        public OrderViewhlder(@NonNull View itemView) {
            super(itemView);
            textordername=itemView.findViewById(R.id.textordername);
            textorderservice=itemView.findViewById(R.id.textorderservice);
            textorderprice=itemView.findViewById(R.id.textorderprice);
            textorderlocation=itemView.findViewById(R.id.textorderlocation);
            acceptbtn=itemView.findViewById(R.id.acceptbtn);
            rejectedbtn=itemView.findViewById(R.id.rejectbtn);
//            recyclerVieworder = itemView.findViewById(R.id.recyclervieworder);

        }
    }
}
