package com.example.tirzah.myapplication;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.example.tirzah.myapplication.utils.CommonUtils;

import java.util.List;

public class AdapterRejected extends RecyclerView.Adapter<AdapterRejected.InformationViewHolder>  {
    private Context context;
    private List<Informationrejected> information;

    public AdapterRejected(Context context, List<Informationrejected> information) {
        this.context = context;
        this.information=information;

    }

    @NonNull
    @Override
    public InformationViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        LayoutInflater inflater=LayoutInflater.from(context);
        View view=inflater.inflate(R.layout.rejectedlist,null);
        InformationViewHolder holder=new InformationViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull InformationViewHolder informationViewHolder, final int i) {
        informationViewHolder.TextName.setText(CommonUtils.fromHtml("Name: ","Murali Krishna"));
        informationViewHolder.TextService.setText(CommonUtils.fromHtml("Service: ","Full Car Wash"));
        informationViewHolder.TextStatus.setText(CommonUtils.fromHtml("Status: ","Confirmed By Driver"));
        informationViewHolder.recyclerViewrejected.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(context,"Position" + i,Toast.LENGTH_LONG).show();
            }
        });
    }

    @Override
    public int getItemCount() {
        return 3;
    }

    class InformationViewHolder extends RecyclerView.ViewHolder{
        TextView TextName,TextService,TextStatus;
        RecyclerView recyclerViewrejected;
        public InformationViewHolder(@NonNull View itemView) {
            super(itemView);
            TextName=itemView.findViewById(R.id.namerejected);
            TextService=itemView.findViewById(R.id.servicerejected);
            TextStatus=itemView.findViewById(R.id.statusrejected);
            recyclerViewrejected=itemView.findViewById(R.id.recyclerviewrejected);
        }
    }
}
