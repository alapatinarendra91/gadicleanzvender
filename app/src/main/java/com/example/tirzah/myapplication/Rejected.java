package com.example.tirzah.myapplication;


import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.List;


/**
 * A simple {@link Fragment} subclass.
 */
public class Rejected extends Fragment {
    List<Informationrejected> informationList;
    AdapterRejected confirm;
    RecyclerView recyclerView;




    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view=inflater.inflate(R.layout.fragment_rejected, container, false);
        informationList=new ArrayList<Informationrejected>();
        recyclerView=(RecyclerView)view.findViewById(R.id.recyclerviewrejected);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));

        informationList.add(new Informationrejected(1,
                "Murali Krishna",
                "Full car service",
                "Rejected by Vendor"));
        confirm=new AdapterRejected(getActivity(),informationList);
        recyclerView.setAdapter(confirm);
        return view;
    }

    public interface OnFragmentInteractionListener {
    }
}
