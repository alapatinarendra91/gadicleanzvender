package com.example.tirzah.myapplication.utils;

import android.text.Html;
import android.text.Spanned;
import android.util.Log;

public class CommonUtils {
    public static Spanned fromHtml(String html, String normal) {
        Spanned result;
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.N) {
            result = Html.fromHtml("<b>"+html+"</b>"+normal, Html.FROM_HTML_MODE_LEGACY);
        } else {
            result = Html.fromHtml("<b>"+html+"</b>"+normal);
        }
        return result;
    }

    /**
     * @param tag
     * @param message
     */
    public static void logPrint(String tag, String message) {
        Log.e(tag, "" + message);
    }

}
