package com.example.tirzah.myapplication.constants;

/**
 * Created by i5 on 01-02-2018.
 */

public class AppConstants {


    public static String displayDateFormate = "dd/MM/yyyy";
    public static String serverDateFormate = "yyyy-MM-dd";

    /* For Shared Prefarenses Only*/
    public static String PREF_NAME = "VENDER_pref";
    public static String gcmRegId = "gcmRegId";
    public static String PREF_user_email = "PREF_user_email";
    public static String PREF_customer_id = "PREF_customer_id";
    public static String PREF_firstTimeLogin = "PREF_firstTimeLogin";


    public static int PERMISSION_REQUEST_Location = 100;

    /*  Used foe Intents Only */
    public static String intent_ComingFrom = "intent_ComingFrom";

}
