package com.example.tirzah.myapplication;


import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;

import java.util.ArrayList;
import java.util.List;


/**
 * A simple {@link Fragment} subclass.
 */
public class MyOrders extends Fragment {
RecyclerView recyclerVieworder;
List<InformationOrders> informationOrders;
AdapterOrder adapterOrder;




    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view=inflater.inflate(R.layout.fragment_my_orders, container, false);
        recyclerVieworder=view.findViewById(R.id.recyclervieworder);
        ImageButton notification=(ImageButton)view.findViewById(R.id.imageButton_orders_notification);
        notification.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent=new Intent(getActivity(),Notifications.class);
                startActivity(intent);

            }
        });
        recyclerVieworder.setHasFixedSize(true);
        recyclerVieworder.setLayoutManager(new LinearLayoutManager(getActivity()));
        informationOrders=new ArrayList<InformationOrders>();
informationOrders.add(new InformationOrders(1,
        "Harish",
        "Full Car Wash Service",
        "Kukatpally",
         2000,
        "Accept",
        "RejectBtn"));
adapterOrder=new AdapterOrder(getActivity(),informationOrders);
recyclerVieworder.setAdapter(adapterOrder);
        return view;
    }

}
