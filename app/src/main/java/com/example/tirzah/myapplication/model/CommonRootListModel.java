package com.example.tirzah.myapplication.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

/**
 * Created by i5 on 06-10-2018.
 */

public class CommonRootListModel {
    @SerializedName("StatusCode")
    @Expose
    private Integer statusCode;
    @SerializedName("StatusText")
    @Expose
    private String statusText;
    @SerializedName("Data")
    @Expose
    private ArrayList<CommonListModel> data = null;

    public Integer getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(Integer statusCode) {
        this.statusCode = statusCode;
    }

    public String getStatusText() {
        return statusText;
    }

    public void setStatusText(String statusText) {
        this.statusText = statusText;
    }

    public ArrayList<CommonListModel> getData() {
        return data;
    }

    public void setData(ArrayList<CommonListModel> data) {
        this.data = data;
    }

}
