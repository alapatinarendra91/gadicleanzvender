package com.example.tirzah.myapplication;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.example.tirzah.myapplication.utils.CommonUtils;

import java.util.List;

public class AdapterConfirm extends RecyclerView.Adapter<AdapterConfirm.InformationViewHolder>  {
    private Context context;
    private List<InformationConfirmed> information;

    public AdapterConfirm(Context context, List<InformationConfirmed> information) {
        this.context = context;
        this.information = information;
    }

    @NonNull
    @Override
    public InformationViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        LayoutInflater inflater=LayoutInflater.from(context);
        View view=inflater.inflate(R.layout.confirmlist,null);
        InformationViewHolder holder=new InformationViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull InformationViewHolder informationViewHolder, final int i) {
        informationViewHolder.TextName.setText(CommonUtils.fromHtml("Name: ","Murali Krishna"));
        informationViewHolder.TextService.setText(CommonUtils.fromHtml("Service: ","Full Car Wash"));
        informationViewHolder.Textprice.setText(CommonUtils.fromHtml("Price: ","2000"));
        informationViewHolder.TextDate.setText(CommonUtils.fromHtml("Date: ","01/01/2018"));
        informationViewHolder.TextStatus.setText(CommonUtils.fromHtml("Status: ","Confirmed By Driver"));
        informationViewHolder.recyclerViewconf.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(context,"Position" + i,Toast.LENGTH_LONG).show();
            }
        });


    }

    @Override
    public int getItemCount() {
        return 10;
    }

    class InformationViewHolder extends RecyclerView.ViewHolder{
        TextView TextName,TextService,TextStatus,TextDate,Textprice;
        RecyclerView recyclerViewconf;


        public InformationViewHolder(@NonNull View itemView) {
            super(itemView);
            TextName=itemView.findViewById(R.id.name);
            TextService=itemView.findViewById(R.id.service);
            TextStatus=itemView.findViewById(R.id.status);
            TextDate=itemView.findViewById(R.id.date);
            Textprice=itemView.findViewById(R.id.price);
            recyclerViewconf = itemView.findViewById(R.id.recyclerviewConfirmed);


        }
    }
}
