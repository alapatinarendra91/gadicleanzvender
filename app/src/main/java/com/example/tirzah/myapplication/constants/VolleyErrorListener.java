package com.example.tirzah.myapplication.constants;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.example.tirzah.myapplication.utils.CommonUtils;

import org.json.JSONObject;

import java.io.UnsupportedEncodingException;

import carwash.ui.IScreen;


/***
 * @author Narendra
 * API Error handle of get@ application
 */

public class VolleyErrorListener implements Response.ErrorListener {
    private final int action;
    private final IScreen screen;
    private ErrorModel errorModel_setdata;

    public VolleyErrorListener(final IScreen screen, final int action) {
        this.screen = screen;
        this.action = action;

    }

    @Override
    public void onErrorResponse(VolleyError error) {
        String str = "";
        int code = 0;
        ErrorModel errorModel = new ErrorModel();

        if (error != null && error.networkResponse != null && error.networkResponse.data != null) {

            try {
                str = new String(error.networkResponse.data, "UTF-8");
                code = error.networkResponse.statusCode;
                CommonUtils.logPrint("Code", "" + code);
                CommonUtils.logPrint("str", "" + str);
                errorModel = jsonToArray(str, code);
                if (errorModel != null) {
//                    if (code == 401)
//                    CommonUtils.mAuthenticationCounter.start();
                    screen.updateUi(false, action, errorModel);
                } else {
                    errorModel = new ErrorModel();
                    if (code >= 402 && code < 500) {
                        errorModel.setStatusText("Bad Request");
                        screen.updateUi(false, action, errorModel);
                    } else if (code >= 500){
                        errorModel.setStatusText("Server error");
                        screen.updateUi(false, action, errorModel);
                    }
                }
                return;
            } catch (UnsupportedEncodingException ex) {
                ex.printStackTrace();
            }
        } else if (screen != null)
            if (error instanceof NoConnectionError) {
                CommonUtils.logPrint("VolleyError", "here1");
                try {
                    CommonUtils.logPrint("Response", "" + error);
                    str = new String(error.networkResponse.data, "UTF-8");
                    CommonUtils.logPrint("VolleyError", str);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                errorModel.setStatusText(Constant.NoConnectionError);
                screen.updateUi(false, action, errorModel);
            } else if (error instanceof AuthFailureError) {
                errorModel.setStatusText(Constant.AuthFailureError);
                screen.updateUi(false, action, errorModel);
            } else if (error instanceof NetworkError) {
                errorModel.setStatusText(Constant.NetworkError);
                screen.updateUi(false, action, errorModel);
            } else if (error instanceof ParseError) {
                errorModel.setStatusText(Constant.ParseError);
                screen.updateUi(false, action, errorModel);
            } else if (error instanceof ServerError) {
                errorModel.setStatusText(Constant.ServerError);
                screen.updateUi(false, action, errorModel);
            } else if (error instanceof TimeoutError) {
                errorModel.setStatusText(Constant.TimeoutError);
                screen.updateUi(false, action, errorModel);
            }
    }

    /***
     *
     * @param jsonInput
     * @param statusCode
     * @return
     */
    private ErrorModel jsonToArray(String jsonInput, int statusCode) {
        try {
            JSONObject jsonObject = new JSONObject(jsonInput);
            errorModel_setdata = new ErrorModel();
            if (jsonObject.has("StatusText"))
                errorModel_setdata.setStatusText(jsonObject.optString("StatusText"));
            if (jsonObject.has("StatusCode"))
                errorModel_setdata.setStatusCode(jsonObject.optInt("StatusCode"));

            return  errorModel_setdata;
        } catch (Exception e) {
            CommonUtils.logPrint("","exception is : "+e.toString());
            return  errorModel_setdata;
//            return_icon null;
        }
    }
}
