package com.example.tirzah.myapplication;

import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MenuItem;

public class MainActivity extends AppCompatActivity implements BottomNavigationView.OnNavigationItemSelectedListener {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        BottomNavigationView view=findViewById(R.id.bottom);
        view.setOnNavigationItemSelectedListener(this);
        LoadFragment(new Home());

    }
    private boolean LoadFragment(Fragment fragment) {
        if (fragment != null) {

            getSupportFragmentManager().beginTransaction().replace(R.id.frame, fragment).commit();

            return true;

        }
        return false;
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
        Fragment selectedfragment = null;
        switch (menuItem.getItemId()) {
            case R.id.home:
                selectedfragment = new Home();
                break;
            case R.id.myorders:
                selectedfragment = new MyOrders();
                break;
            case R.id.drivers:
                selectedfragment=new Drivers();
                break;


            case R.id.dashboard:
                selectedfragment = new Dashboard();
                break;
            case R.id.account:
                selectedfragment=new Account();
                break;


        }







        return LoadFragment(selectedfragment);
    }
}
