package com.example.tirzah.myapplication.utils;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Created by ANDROID on 8/1/2016.
 */
public class PrefUtil {
    // Avoid magic numbers.
//    private static final int MAX_SIZE = 1;

    public static void putString(Context context, String key, String value, String pref) {
        if(context != null && key != null) {
            if(pref != null && !pref.isEmpty()) {
                context.getSharedPreferences(pref, 0).edit().putString(key, value).apply();
            } else {
                PreferenceManager.getDefaultSharedPreferences(context).edit().putString(key, value).apply();
            }

        }
    }

    public static String getString(Context context, String key, String pref) {
        return context != null && key != null?(pref != null && !pref.isEmpty()?context.getSharedPreferences(pref, 0).getString(key, (String)null): PreferenceManager.getDefaultSharedPreferences(context).getString(key, (String)null)):null;
    }

    public static void putInt(Context context, String key, int value, String pref) {
        if (context==null || key == null) { return; }
        if (pref==null || pref.isEmpty()) {
            PreferenceManager.getDefaultSharedPreferences(context).edit().putInt(key, value).apply();
        } else {
            context.getSharedPreferences(pref, Context.MODE_PRIVATE).edit().putInt(key, value).apply();
        }
    }

    public static int getInt(Context context, String key, String pref) {
        if (context==null || key == null) { return 0; }
        if (pref==null || pref.isEmpty()) {
            return PreferenceManager.getDefaultSharedPreferences(context).getInt(key, 0);
        } else {
            return context.getSharedPreferences(pref, Context.MODE_PRIVATE).getInt(key, 0);
        }
    }

    public static void putBool(Context context, String key, boolean value, String pref) {
        if (context==null || key == null) { return; }
        if (pref==null || pref.isEmpty()) {
            PreferenceManager.getDefaultSharedPreferences(context).edit().putBoolean(key, value).apply();
        } else {
            context.getSharedPreferences(pref, Context.MODE_PRIVATE).edit().putBoolean(key, value).apply();
        }
    }

    public static boolean getBool(Context context, String key, String pref) {
        if (context==null || key == null) { return false; }
        if (pref==null || pref.isEmpty()) {
            return PreferenceManager.getDefaultSharedPreferences(context).getBoolean(key, false);
        } else {
            return context.getSharedPreferences(pref, Context.MODE_PRIVATE).getBoolean(key, false);
        }
    }

    public static void clearAllSharedPrefarences(Context context, String pref){
        context.getSharedPreferences(pref, 0).edit().clear().apply();
    }
    public static void clearSharedPrefarence(Context context, String pref, String key){
        context.getSharedPreferences(pref, 0).edit().remove(key).commit();
    }

    public static void storeList(Context context, String pref_name, String key, List countries) {

        SharedPreferences settings;
        SharedPreferences.Editor editor;
        settings = context.getSharedPreferences(pref_name, Context.MODE_PRIVATE);
        editor = settings.edit();
        Gson gson = new Gson();
        String jsonFavorites = gson.toJson(countries);
        editor.putString(key, jsonFavorites);
        editor.apply();
    }

    public static ArrayList<String> loadList(Context context, String pref_name, String key) {

        SharedPreferences settings;
        List<String> favorites;
        settings = context.getSharedPreferences(pref_name, Context.MODE_PRIVATE);
        if (settings.contains(key)) {
            String jsonFavorites = settings.getString(key, null);
            Gson gson = new Gson();
            String[] favoriteItems = gson.fromJson(jsonFavorites, String[].class);
            favorites = Arrays.asList(favoriteItems);
            favorites = new ArrayList<>(favorites);
        } else
            return null;
        return (ArrayList<String>) favorites;
    }

    public static void addList(Context context, String pref_name, String key, String searchItem) {
        List<String> favorites = loadList(context, pref_name, key);
        if (favorites == null)
            favorites = new ArrayList<>();

        if(favorites.contains(searchItem)){
            favorites.remove(searchItem);
        }
        favorites.add(searchItem);

        if(favorites.size() > 5) {
            favorites.remove(0);
        }

        storeList(context, pref_name, key, favorites);

    }



}


