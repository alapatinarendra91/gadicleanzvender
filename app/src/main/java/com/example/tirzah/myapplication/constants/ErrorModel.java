package com.example.tirzah.myapplication.constants;


import carwash.model.BaseModel;

/**
 * Created by Ankur on 15/06/15.
 */
public class ErrorModel extends BaseModel {
   private int StatusCode;
   private String StatusText;


    public int getStatusCode() {
        return StatusCode;
    }

    public void setStatusCode(int StatusCode) {
        this.StatusCode = StatusCode;
    }

    public String getStatusText() {
        return StatusText;
    }

    public void setStatusText(String statusText) {
        StatusText = statusText;
    }
}
