package com.example.tirzah.myapplication;


import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.TextView;


/**
 * A simple {@link Fragment} subclass.
 */
public class Account extends Fragment {


    public Account() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view=inflater.inflate(R.layout.fragment_account, container, false);
        TextView logout=(TextView)view.findViewById(R.id.text_logout);
        TextView profile=(TextView)view.findViewById(R.id.text_profile);
        TextView past_orders=(TextView)view.findViewById(R.id.text_past_orders);
        TextView terms_conditions=(TextView)view.findViewById(R.id.text_terms_conditions);
        final TextView About_us=(TextView)view.findViewById(R.id.text_about);
        About_us.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(getActivity(),AboutUs.class);
                startActivity(intent);
            }
        });
        terms_conditions.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
            Intent intent=new Intent(getActivity(),Terms_and_conditions.class);
            startActivity(intent);
            }
        });
        final TextView present_orders=(TextView)view.findViewById(R.id.text_present_orders);
        present_orders.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(getActivity(),Present_Orders.class);
                startActivity(intent);
            }
        });
        past_orders.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(getActivity(),Past_Orders.class);
                startActivity(intent);
            }
        });
        profile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(getActivity(),Profile.class);
                startActivity(intent);
            }
        });
        ImageButton imageButton = (ImageButton)view.findViewById(R.id.imagebutton_account_notification);
        imageButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(getActivity(),Notifications.class);
                startActivity(intent);
            }
        });

        logout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(getActivity(),Login.class);
                startActivity(intent);
            }
        });
        return view;
    }

}
