package com.example.tirzah.myapplication;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v4.app.FragmentActivity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.example.tirzah.myapplication.utils.CommonUtils;

import java.util.List;

public class DriverAdaptor extends RecyclerView.Adapter<DriverAdaptor.DriverInformationViewHolder> {
    private Context context;

    public DriverAdaptor(FragmentActivity activity, List<DriverInformation> driverInformations) {
        this.driverInformations = driverInformations;
        this.context=activity;
    }

    private List<DriverInformation> driverInformations;
    @NonNull
    @Override
    public DriverInformationViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        LayoutInflater inflater=LayoutInflater.from(context);
        View view=inflater.inflate(R.layout.drivers_list,null);
        DriverInformationViewHolder holder=new DriverInformationViewHolder(view);

        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull DriverInformationViewHolder driverInformationViewHolder, int i) {
        driverInformationViewHolder.driverName.setText(CommonUtils.fromHtml("Driver Name: ","Manoj Kumar"));
        driverInformationViewHolder.driverLocation.setText(CommonUtils.fromHtml("Location: ","Dilshuknagar" + i));

    }

    @Override
    public int getItemCount() {
        return 10;
    }

    class DriverInformationViewHolder extends RecyclerView.ViewHolder {
        TextView driverName,driverLocation;
        Button AssignBtn,RejectBtn;
        RelativeLayout relativeLayout;

        public DriverInformationViewHolder(@NonNull View itemView) {
            super(itemView);
            driverName=itemView.findViewById(R.id.driverName);
            driverLocation=itemView.findViewById(R.id.driverLocation);
            AssignBtn=itemView.findViewById(R.id.assignbtn);
            RejectBtn=itemView.findViewById(R.id.rejectbtn);

        }
    }
}
