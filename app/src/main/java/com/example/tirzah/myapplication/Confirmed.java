package com.example.tirzah.myapplication;


import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.List;


/**
 * A simple {@link Fragment} subclass.
 */
public class Confirmed extends Fragment {
    List<InformationConfirmed> informationList;
    AdapterConfirm confirm;
    RecyclerView recyclerView;



    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view=inflater.inflate(R.layout.fragment_confirmed, container, false);
        informationList=new ArrayList<InformationConfirmed>();
        recyclerView=(RecyclerView)view.findViewById(R.id.recyclerviewConfirmed);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));

       informationList.add(new InformationConfirmed("1",
               "Murali Krishna",
               "Full Car Service",
               "2000",
               "01/01/2018",
               "Confirmed by Driver"));
        confirm=new AdapterConfirm(getActivity(),informationList);
        recyclerView.setAdapter(confirm);


        return view;
    }


    public interface OnFragmentInteractionListener {
    }
}
